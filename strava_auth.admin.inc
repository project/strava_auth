<?php

/**
 * @file
 * Strava authentication admin forms.
 */

/**
 * Strava authentication admin settings form.
 */
function strava_auth_admin_settings($form, &$form_state) {
  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['accounts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account settings'),
    '#group' => 'vertical_tabs',
  );
  $form['accounts']['strava_auth_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Strava authentication'),
    '#default_value' => variable_get('strava_auth_enable'),
  );
  $roles = array_map('check_plain', user_roles(TRUE));
  $checkbox_authenticated = array(
    '#type' => 'checkbox',
    '#title' => $roles[DRUPAL_AUTHENTICATED_RID],
    '#default_value' => TRUE,
    '#disabled' => TRUE,
  );
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  $form['accounts']['strava_auth_user_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Strava user roles'),
    '#description' => t('Missing user roles are assigned upon login.'),
    '#default_value' => variable_get('strava_auth_user_roles', array()),
    '#options' => $roles,
    DRUPAL_AUTHENTICATED_RID => $checkbox_authenticated,
  );
  $form['accounts']['strava_auth_login_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirection path after login'),
    '#default_value' => variable_get('strava_auth_login_redirect', 'user'),
  );

  $form['connection'] = array(
    '#type' => 'fieldset',
    '#title' => t('Connection settings'),
    '#group' => 'vertical_tabs',
  );
  $form['connection']['info'] = array(
    '#markup' => t('Configure API access with credentials as given on <a href="!url">My API Application</a>.', array(
      '!url' => 'https://www.strava.com/settings/api',
    )),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['connection']['strava_auth_api_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#default_value' => variable_get('strava_auth_api_client_id'),
    '#size' => 5,
    '#required' => TRUE,
  );
  $form['connection']['strava_auth_api_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#default_value' => variable_get('strava_auth_api_client_secret'),
    '#size' => 50,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
