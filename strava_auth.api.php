<?php

/**
 * @file
 * Hooks provided by the Strava Authentication module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * The Strava user has logged in.
 *
 * @param object $user
 *   The Drupal user object.
 * @param object $details
 *   Strava OAuth user details.
 * @param object $token
 *   Strava OAuth token object.
 */
function hook_strava_auth_user_login($user, $details, $token) {
  $edit = array();
  $edit['field_first_name'][LANGUAGE_NONE][0]['value'] = $details->firstName;
  user_save($user, $edit);
}

/**
 * @} End of "addtogroup hooks".
 */
