INTRODUCTION
------------

This small module ables users to sign in into a Drupal 7 website using a
Strava.com account. It makes use of the excellent StravaPHP
(https://github.com/basvandorst/StravaPHP) library to implement server-side
OAuth authentication and to access the Strava API (http://strava.github.io/api/).

INSTALLATION
------------

Using the supplied composer.json file, install the StravaPHP library and its
dependencies and include Composer's autoload.php into Drupal.

The X Autoload module is currently required for loading in the Factory API
class. Simply enable the strava_auth module.

CONFIGURATION
-------------

Go to admin/config/people/strava-auth and fill in the Strava API client ID and
secret. These can be retrieved after creating an API Application at
https://www.strava.com/settings/api.

Then, select which roles the Strava users have to be assigned to automatically
and enable authentication.

DEVELOPING
----------

Currently, the module does not implement a custom block or alter the login
block. You must direct anonymous users to the user/strava-login path, after
which they will be redirected to Strava.com to login.

A hook is available which will be triggered once a Strava user has logged in
successfully. Please see strava_auth.api.php for the specification.

The StravaPHP library can easily be accessed using the supplied Factory class.

  $factory = Drupal\strava_auth\Strava\Factory::getInstance();
  $api_client = $factory->getApiClient($user_token);

The user token needs to be passed explicitly only when not executed in a Strava
user's session.

See https://github.com/basvandorst/StravaPHP/blob/master/README.md for an
overview of the available methods on Strava\API\Client.

MAINTAINERS
-----------

Current maintainers:

  * Dietrich Moerman (dietr_ch) - https://www.drupal.org/u/dietr_ch

This module is sponsored by Wunderkraut Benelux (http://wunderkraut.be/).
