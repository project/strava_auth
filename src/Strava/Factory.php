<?php

/**
 * @file
 * Contains the Drupal Strava factory.
 */

namespace Drupal\strava_auth\Strava;

use Strava\API\Factory as StravaFactory;

/**
 * Drupal Strava factory.
 */
class Factory {
  protected $clientId;
  protected $clientSecret;
  protected $factory;

  /**
   * Constructor.
   */
  protected function __construct() {
    $this->clientId = variable_get('strava_auth_api_client_id');
    $this->clientSecret = variable_get('strava_auth_api_client_secret');
    $this->factory = new StravaFactory();
  }

  /**
   * Singleton instance.
   *
   * @returns Factory
   *   Factory instance.
   */
  public static function getInstance() {
    static $instance;
    if (!isset($instance)) {
      $instance = new self();
    }
    return $instance;
  }

  /**
   * Get OAuth client.
   *
   * @returns Strava\API\OAuth OAuth client.
   */
  public function getOauthClient() {
    $current_url = url(current_path(), array('absolute' => TRUE));
    return $this->factory->getOAuthClient($this->clientId, $this->clientSecret, $current_url);
  }

  /**
   * Get API client.
   *
   * @param string $token
   *   OAuth token (optional; taken from session for Strava users).
   *
   * @returns Strava\API\Client API client.
   */
  public function getApiClient($token = NULL) {
    if (empty($token)) {
      if (empty($_SESSION['strava_auth_token'])) {
        throw new UnexpectedValueException('No OAuth token found or passed.');
      }
      $token = $_SESSION['strava_auth_token'];
    }
    return $this->factory->getAPIClient($token);
  }

}
